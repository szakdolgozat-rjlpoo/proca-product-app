INSERT INTO categories (name, created_at, updated_at)
VALUES ('Mug', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Cup', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Honey Pot', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Camera', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Lock', '1970-01-01 00:00:01', '1970-01-01 00:00:01')
;

INSERT INTO products (name, description, img, created_at, updated_at)
VALUES ('Mug 1', 'Description of Mug 1', 'https://picsum.photos/id/113/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Mug 2', 'Description of Mug 2', 'https://picsum.photos/id/30/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Mug 3', 'Description of Mug 3', 'https://picsum.photos/id/326/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Mug 4', 'Description of Mug 4', 'https://picsum.photos/id/366/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Cup 1', 'Description of Cup 1', 'https://picsum.photos/id/225/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Camera 1', 'Description of Camera 1', 'https://picsum.photos/id/250/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Camera 2', 'Description of Camera 2', 'https://picsum.photos/id/319/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Honey Pot 1', 'Description of Honey Pot 1', 'https://picsum.photos/id/312/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('Lock 1', 'Description of Lock 1', 'https://picsum.photos/id/336/300/300?blur=2', '1970-01-01 00:00:01', '1970-01-01 00:00:01')
;

INSERT INTO category_product (product_id, category_id)
VALUES ((SELECT id FROM products WHERE name = 'Mug 1'), (SELECT id FROM categories WHERE name = 'Mug')),
       ((SELECT id FROM products WHERE name = 'Mug 2'), (SELECT id FROM categories WHERE name = 'Mug')),
       ((SELECT id FROM products WHERE name = 'Mug 3'), (SELECT id FROM categories WHERE name = 'Mug')),
       ((SELECT id FROM products WHERE name = 'Mug 4'), (SELECT id FROM categories WHERE name = 'Mug')),
       ((SELECT id FROM products WHERE name = 'Cup 1'), (SELECT id FROM categories WHERE name = 'Cup')),
       ((SELECT id FROM products WHERE name = 'Camera 1'), (SELECT id FROM categories WHERE name = 'Camera')),
       ((SELECT id FROM products WHERE name = 'Camera 2'), (SELECT id FROM categories WHERE name = 'Camera')),
       ((SELECT id FROM products WHERE name = 'Honey Pot 1'), (SELECT id FROM categories WHERE name = 'Honey Pot')),
       ((SELECT id FROM products WHERE name = 'Lock 1'), (SELECT id FROM categories WHERE name = 'Lock'))
;

INSERT INTO prices (currency_code, value, product_id, supplier, created_at, updated_at)
VALUES ('USD', 24.99, (SELECT id FROM products WHERE name = 'Mug 1'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 29.99, (SELECT id FROM products WHERE name = 'Mug 1'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 24.99, (SELECT id FROM products WHERE name = 'Mug 2'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 29.99, (SELECT id FROM products WHERE name = 'Mug 2'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 24.99, (SELECT id FROM products WHERE name = 'Mug 3'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 29.99, (SELECT id FROM products WHERE name = 'Mug 3'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 24.99, (SELECT id FROM products WHERE name = 'Mug 4'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 29.99, (SELECT id FROM products WHERE name = 'Mug 4'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 14.99, (SELECT id FROM products WHERE name = 'Cup 1'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 19.99, (SELECT id FROM products WHERE name = 'Cup 1'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 399.99, (SELECT id FROM products WHERE name = 'Camera 1'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 389.99, (SELECT id FROM products WHERE name = 'Camera 1'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 524.99, (SELECT id FROM products WHERE name = 'Camera 2'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 499.99, (SELECT id FROM products WHERE name = 'Camera 2'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 14.99, (SELECT id FROM products WHERE name = 'Honey Pot 1'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 19.99, (SELECT id FROM products WHERE name = 'Honey Pot 1'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 14.99, (SELECT id FROM products WHERE name = 'Lock 1'), '5dae1eb62742da3d8b7163f3', '1970-01-01 00:00:01', '1970-01-01 00:00:01'),
       ('USD', 9.99, (SELECT id FROM products WHERE name = 'Lock 1'), '5dae225a8a7d1d18eff0f14e', '1970-01-01 00:00:01', '1970-01-01 00:00:01')
;

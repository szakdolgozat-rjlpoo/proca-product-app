package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.app.domain.entity.Product;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.api.ProductResponse;

import java.util.*;
import java.util.stream.Collectors;

public class ProductResponseConverter implements Converter<Product, ProductResponse> {

    private final CategoryResponseConverter categoryResponseConverter;

    private final PriceResponseConverter priceResponseConverter;

    public ProductResponseConverter(
            CategoryResponseConverter categoryResponseConverter,
            PriceResponseConverter priceResponseConverter) {
        this.categoryResponseConverter = categoryResponseConverter;
        this.priceResponseConverter = priceResponseConverter;
    }

    @Override
    public ProductResponse convert(@Nullable Product source) {
        if (source == null) {
            return null;
        }

        return ProductResponse.builder()
                .id(source.getId())
                .name(source.getName())
                .description(source.getDescription())
                .img(source.getImg())
                .categories(this.getCategories(source))
                .prices(this.getPrices(source))
                .build();
    }

    private List<PriceResponse> getPrices(Product source) {
        return Optional.ofNullable(source.getPrices())
                .orElse(new ArrayList<>())
                .stream()
                .map(priceResponseConverter::convert)
                .collect(Collectors.toList());
    }

    private Set<String> getCategories(Product source) {
        return Optional.ofNullable(source.getCategories())
                .orElse(new HashSet<>())
                .stream()
                .map(categoryResponseConverter::convert)
                .collect(Collectors.toSet());
    }
}

package xyz.nergal.proca.product.app.service.convert;

import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.stereotype.Component;
import xyz.nergal.proca.product.app.service.convert.converter.*;

@Component
public class EntityConversionService extends GenericConversionService {

    /**
     * Create a new {@code EntityConversionService} with the set of
     * {@linkplain EntityConversionService#addDefaultConverters(ConverterRegistry) default converters}.
     */
    public EntityConversionService() {
        addDefaultConverters(this);
    }

    /**
     * Add converters appropriate for entities.
     * @param converterRegistry the registry of converters to add to
     */
    private static void addDefaultConverters(ConverterRegistry converterRegistry) {
        CategoryConverter categoryConverter = new CategoryConverter();
        PriceConverter priceConverter = new PriceConverter();
        ProductConverter productConverter = new ProductConverter(categoryConverter);

        converterRegistry.addConverter(categoryConverter);
        converterRegistry.addConverter(priceConverter);
        converterRegistry.addConverter(productConverter);

        CategoryResponseConverter categoryResponseConverter = new CategoryResponseConverter();
        PriceResponseConverter priceResponseConverter = new PriceResponseConverter();
        ProductResponseConverter productResponseConverter = new ProductResponseConverter(categoryResponseConverter, priceResponseConverter);

        converterRegistry.addConverter(categoryResponseConverter);
        converterRegistry.addConverter(priceResponseConverter);
        converterRegistry.addConverter(productResponseConverter);
    }
}

package xyz.nergal.proca.product.app.service.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.lang.NonNull;
import xyz.nergal.proca.product.app.domain.entity.Price;

public class PriceChangedEvent extends ApplicationEvent {

    public PriceChangedEvent(@NonNull Price source) {
        super(source);
    }
}

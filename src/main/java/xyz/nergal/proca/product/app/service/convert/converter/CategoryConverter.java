package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.app.domain.entity.Category;

public class CategoryConverter implements Converter<String, Category> {

    @Override
    public Category convert(@Nullable String source) {
        if (source == null) {
            return null;
        }

        return Category.builder()
                .name(source)
                .build();
    }
}

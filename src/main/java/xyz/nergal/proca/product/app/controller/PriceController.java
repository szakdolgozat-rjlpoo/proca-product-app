package xyz.nergal.proca.product.app.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.service.PriceService;

@RestController
@RequestMapping("/products/{product}/prices")
public class PriceController {

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Page<PriceResponse>>> readAll(@PathVariable Long product, Pageable pageable) {
        return priceService.list(product, pageable)
                .map(priceResponses -> ResponseEntity.ok().body(priceResponses))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<PriceResponse>> read(@PathVariable Long product, @PathVariable Long id) {
        return priceService.get(product, id)
                .map(priceResponse -> ResponseEntity.ok().body(priceResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}

package xyz.nergal.proca.product.app.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.controller.validation.ProductRequestValidator;
import xyz.nergal.proca.product.app.service.ProductService;

import javax.validation.Valid;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    private final ProductRequestValidator productRequestValidator;

    public ProductController(
            ProductService productService,
            ProductRequestValidator productRequestValidator) {
        this.productService = productService;
        this.productRequestValidator = productRequestValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(productRequestValidator);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Page<ProductResponse>> readAll(Pageable pageable) {
        return productService.list(pageable);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<ProductResponse>> read(@PathVariable Long id) {
        return productService.get(id)
                .map(productResponse -> ResponseEntity.ok().body(productResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<ProductResponse>> create(@RequestBody @Valid ProductRequest request) {
        return productService.store(request)
                .map(productResponse -> ResponseEntity.status(HttpStatus.CREATED).body(productResponse));
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<ProductResponse>> update(@PathVariable Long id, @RequestBody @Valid ProductRequest request) {
        return productService.update(id, request)
                .map(productResponse -> ResponseEntity.ok().body(productResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Object>> delete(@PathVariable Long id) {
        return productService.delete(id)
                .map(productResponse -> ResponseEntity.noContent().build())
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}

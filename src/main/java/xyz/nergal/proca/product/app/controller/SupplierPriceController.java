package xyz.nergal.proca.product.app.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.service.PriceService;

import javax.validation.Valid;

@RestController
@RequestMapping("/products/{product}/suppliers/{supplier}/prices")
public class SupplierPriceController {

    private final PriceService priceService;

    public SupplierPriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Page<PriceResponse>>> readAll(@PathVariable Long product, @PathVariable String supplier, Pageable pageable) {
        return priceService.list(product, supplier, pageable)
                .map(priceResponses -> ResponseEntity.ok().body(priceResponses))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<PriceResponse>> read(@PathVariable Long product, @PathVariable String supplier, @PathVariable Long id) {
        return priceService.get(product, supplier, id)
                .map(priceResponse -> ResponseEntity.ok().body(priceResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<PriceResponse>> create(@PathVariable Long product, @PathVariable String supplier, @RequestBody @Valid PriceRequest request) {
        return priceService.store(product, supplier, request)
                .map(priceResponse -> ResponseEntity.status(HttpStatus.CREATED).body(priceResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<PriceResponse>> update(@PathVariable Long product, @PathVariable String supplier, @PathVariable Long id, @RequestBody @Valid PriceRequest request) {
        return priceService.update(product, supplier, id, request)
                .map(priceResponse -> ResponseEntity.ok().body(priceResponse))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}

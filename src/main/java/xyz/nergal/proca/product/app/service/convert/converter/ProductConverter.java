package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.app.domain.entity.Category;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ProductConverter implements Converter<ProductRequest, Product> {

    private final CategoryConverter categoryConverter;

    public ProductConverter(CategoryConverter categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    @Override
    public Product convert(@Nullable ProductRequest source) {
        if (source == null) {
            return null;
        }

        return Product.builder()
                .name(source.getName())
                .description(source.getDescription())
                .img(source.getImg())
                .categories(getCategories(source))
                .build();
    }

    private Set<Category> getCategories(ProductRequest source) {
        return Optional.ofNullable(source.getCategories())
                .orElse(Collections.emptySet())
                .stream()
                .map(categoryConverter::convert)
                .collect(Collectors.toSet());
    }
}

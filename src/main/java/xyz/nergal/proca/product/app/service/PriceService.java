package xyz.nergal.proca.product.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.entity.Product;

public interface PriceService {

    /**
     * Return a list of prices for the given page.
     *
     * @param productId the id of the owner {@link Product} entity.
     * @param pageable  the pageable information.
     * @return List of prices.
     */
    Mono<Page<PriceResponse>> list(Long productId, Pageable pageable);

    /**
     * Return a product matching the given id.
     *
     * @param productId the id of the owner {@link Product} entity.
     * @param id        the id of the expected product.
     * @return a price matching the given id.
     */
    Mono<PriceResponse> get(Long productId, Long id);

    /**
     * Return a list of prices for the given page.
     *
     * @param productId the id of the owner {@link Product} entity.
     * @param supplierId the id of the supplier.
     * @param pageable  the pageable information.
     * @return List of prices.
     */
    Mono<Page<PriceResponse>> list(Long productId, String supplierId, Pageable pageable);

    /**
     * Return a product matching the given id.
     *
     * @param productId the id of the owner {@link Product} entity.
     * @param supplierId the id of the supplier.
     * @param id        the id of the expected product.
     * @return a price matching the given id.
     */
    Mono<PriceResponse> get(Long productId, String supplierId, Long id);

    /**
     * Store a price.
     *
     * @param productId  the id of the owner {@link Product} entity.
     * @param supplierId the id of the supplier.
     * @param request    the price details.
     * @return The stored price.
     */
    Mono<PriceResponse> store(Long productId, String supplierId, PriceRequest request);

    /**
     * Update a product.
     *
     * @param productId  the id of the owner {@link Product} entity.
     * @param supplierId the id of the supplier.
     * @param request    the price details.
     * @return The updated price.
     */
    Mono<PriceResponse> update(Long productId, String supplierId, Long id, PriceRequest request);
}

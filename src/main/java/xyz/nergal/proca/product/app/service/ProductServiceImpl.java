package xyz.nergal.proca.product.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.domain.CategoryRepository;
import xyz.nergal.proca.product.app.domain.ProductRepository;
import xyz.nergal.proca.product.app.domain.entity.Category;
import xyz.nergal.proca.product.app.domain.entity.Product;
import xyz.nergal.proca.product.app.service.convert.EntityConversionService;
import xyz.nergal.proca.product.app.service.exception.CategoryNotFoundException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final CategoryRepository categoryRepository;

    private final ProductRepository productRepository;

    private final EntityConversionService entityConversionService;

    public ProductServiceImpl(
            CategoryRepository categoryRepository,
            ProductRepository productRepository,
            EntityConversionService entityConversionService) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.entityConversionService = entityConversionService;
    }

    @Override
    public Mono<Page<ProductResponse>> list(Pageable pageable) {
        return Mono.just(productRepository.findAllByDeletedAtIsNull(pageable))
                .map(products -> products.map(product -> entityConversionService.convert(product, ProductResponse.class)));
    }

    @Override
    public Mono<ProductResponse> get(Long id) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(id))
                .flatMap(product -> Mono.justOrEmpty(entityConversionService.convert(product, ProductResponse.class)));
    }

    @Override
    public Mono<ProductResponse> store(ProductRequest request) {
        return Mono.just(request)
                .flatMap(productRequest -> Mono.justOrEmpty(entityConversionService.convert(productRequest, Product.class)))
                .flatMap(product -> this.resolveCategories(product)
                        .map(objects -> {
                            product.setCategories(new HashSet<>(objects));
                            return product;
                        }))
                .map(productRepository::save)
                .flatMap(product -> Mono.justOrEmpty(entityConversionService.convert(product, ProductResponse.class)));
    }

    @Override
    public Mono<ProductResponse> update(Long id, ProductRequest request) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(id))
                .zipWith(Mono.just(request)
                        .flatMap(productRequest -> Mono.justOrEmpty(entityConversionService.convert(productRequest, Product.class))))
                .map(tuple -> {
                    tuple.getT2().setId(tuple.getT1().getId());
                    tuple.getT2().setCreatedAt(tuple.getT1().getCreatedAt());

                    return tuple.getT2();
                })
                .flatMap(product -> this.resolveCategories(product)
                        .map(objects -> {
                            product.setCategories(new HashSet<>(objects));
                            return product;
                        }))
                .map(productRepository::save)
                .flatMap(product -> Mono.justOrEmpty(entityConversionService.convert(product, ProductResponse.class)));
    }

    @Override
    public Mono<ProductResponse> delete(Long id) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(id))
                .doOnNext(product -> product.setDeletedAt(LocalDateTime.now()))
                .map(productRepository::save)
                .flatMap(product -> Mono.justOrEmpty(entityConversionService.convert(product, ProductResponse.class)));
    }

    private Mono<List<Category>> resolveCategories(Product product) {
        return Flux.fromIterable(Optional.ofNullable(product.getCategories()).orElse(Collections.emptySet()))
                .map(category -> categoryRepository.findByName(category.getName()))
                .concatMap(category -> Mono.justOrEmpty(category)
                        .switchIfEmpty(Mono.error(new CategoryNotFoundException())))
                .collectList();
    }
}

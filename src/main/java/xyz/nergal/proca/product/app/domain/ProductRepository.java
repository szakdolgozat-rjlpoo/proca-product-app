package xyz.nergal.proca.product.app.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    /**
     * Returns a {@link Page} of active (not soft deleted) entities meeting the paging restriction provided
     * in the {@code Pageable} object.
     *
     * @param pageable paging restriction.
     * @return a page of entities.
     */
    Page<Product> findAllByDeletedAtIsNull(Pageable pageable);

    /**
     * Retrieves an active (not soft deleted) entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the active (not soft deleted) entity with the given id or {@literal Optional#empty()} if none found.
     */
    Optional<Product> findByIdAndDeletedAtIsNull(Long id);
}

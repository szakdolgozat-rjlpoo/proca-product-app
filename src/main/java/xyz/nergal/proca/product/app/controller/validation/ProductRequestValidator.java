package xyz.nergal.proca.product.app.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import xyz.nergal.proca.product.api.ProductRequest;

import java.util.Iterator;
import java.util.Set;

@Component
public class ProductRequestValidator implements Validator {

    private final CategoryValidator categoryValidator;

    public ProductRequestValidator(CategoryValidator categoryValidator) {
        this.categoryValidator = categoryValidator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ProductRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductRequest productRequest = (ProductRequest) target;

        Set<String> categories = productRequest.getCategories();

        if (categories == null || categories.isEmpty()) {
            return;
        }

        int index = 0;
        for (Iterator<String> it = categories.iterator(); it.hasNext(); index++) {
            String next = it.next();

            try {
                errors.pushNestedPath("categories[" + index + "]");
                ValidationUtils.invokeValidator(this.categoryValidator, next, errors);
            } finally {
                errors.popNestedPath();
            }
        }
    }
}

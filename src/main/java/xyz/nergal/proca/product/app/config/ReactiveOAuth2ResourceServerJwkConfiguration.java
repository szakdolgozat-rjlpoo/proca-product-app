package xyz.nergal.proca.product.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.*;
import xyz.nergal.proca.product.app.oauth2.AudienceValidator;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class ReactiveOAuth2ResourceServerJwkConfiguration {

    private static Clock jwtTimestampValidatorClock = Clock.systemUTC();

    /**
     * Set the {@link Clock} used by {@link JwtTimestampValidator} for validating jwt timestamp.
     *
     * @param clock a clock instance
     */
    public static void setJwtTimestampValidatorClock(Clock clock) {
        jwtTimestampValidatorClock = clock;
    }

    @Bean
    @ConditionalOnProperty("auth0.audience")
    public ReactiveJwtDecoder jwtDecoder(@Value("${auth0.audience}") String audience, OAuth2ResourceServerProperties properties) {
        NimbusReactiveJwtDecoder jwtDecoder = new NimbusReactiveJwtDecoder(properties.getJwt().getJwkSetUri());
        String issuerUri = properties.getJwt().getIssuerUri();

        jwtDecoder.setJwtValidator(this.createValidator(audience, issuerUri));

        return jwtDecoder;
    }

    private OAuth2TokenValidator<Jwt> createValidator(String audience, String issuer) {
        List<OAuth2TokenValidator<Jwt>> validators = new ArrayList<>();
        validators.add(new AudienceValidator(audience));
        validators.add(jwtTimestampValidator());
        if (issuer != null) {
            validators.add(new JwtIssuerValidator(issuer));
        }
        return new DelegatingOAuth2TokenValidator<>(validators);
    }

    private OAuth2TokenValidator<Jwt> jwtTimestampValidator() {
        JwtTimestampValidator jwtTimestampValidator = new JwtTimestampValidator();
        jwtTimestampValidator.setClock(jwtTimestampValidatorClock);
        return jwtTimestampValidator;
    }
}

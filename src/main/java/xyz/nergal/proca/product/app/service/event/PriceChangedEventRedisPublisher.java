package xyz.nergal.proca.product.app.service.event;

import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import xyz.nergal.proca.product.api.PriceChangeResponse;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.service.convert.EntityConversionService;

@Component
public class PriceChangedEventRedisPublisher implements ApplicationListener<PriceChangedEvent> {

    private static final String DESTINATION = "price-change";

    private final ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    private final EntityConversionService entityConversionService;

    public PriceChangedEventRedisPublisher(
            ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate,
            EntityConversionService entityConversionService) {
        this.reactiveRedisTemplate = reactiveRedisTemplate;
        this.entityConversionService = entityConversionService;
    }

    @Async
    @Override
    public void onApplicationEvent(@NonNull PriceChangedEvent event) {
        Price source = (Price) event.getSource();
        Assert.notNull(source, "Source of event cannot be null");

        reactiveRedisTemplate.convertAndSend(DESTINATION, this.buildPriceChangeResponse(source))
                .subscribe();
    }

    private PriceChangeResponse buildPriceChangeResponse(Price source) {
        PriceResponse priceResponse = entityConversionService.convert(source, PriceResponse.class);

        return PriceChangeResponse.builder()
                .productId(source.getProduct().getId())
                .priceResponse(priceResponse)
                .build();
    }
}

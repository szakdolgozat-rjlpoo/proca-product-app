package xyz.nergal.proca.product.app.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Price of a product.
 *
 * A price belongs to a product. (N to 1)
 */
@Entity
@Table(name = "prices")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Price {

    /**
     * Identifier of a price.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Currency code of a price.
     *
     * The currency code is an ISO 3 representation of a currency.
     */
    @Column(nullable = false)
    private String currencyCode;

    /**
     * Value of a price.
     *
     * Not an atomic field, must be represented with a currency code.
     */
    @Column(nullable = false)
    private BigDecimal value;

    /**
     * Supplier of a price.
     *
     * Contains the ObjectId of a supplier.
     */
    @Column(nullable = false)
    private String supplier;

    /**
     * Creation time of a price.
     */
    @CreationTimestamp
    private LocalDateTime createdAt;

    /**
     * Update time of a price.
     */
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    /**
     * The product which the price belongs to.
     */
    @ManyToOne(optional = false)
    private Product product;
}

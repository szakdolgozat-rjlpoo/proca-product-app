package xyz.nergal.proca.product.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.csrf().disable()
                .authorizeExchange(exchanges ->
                        exchanges
                                .pathMatchers(HttpMethod.POST, "/products").hasAuthority("SCOPE_product:create")
                                .pathMatchers(HttpMethod.PUT, "/products/*").hasAuthority("SCOPE_product:update")
                                .pathMatchers(HttpMethod.DELETE, "/products/*").hasAuthority("SCOPE_product:delete")
                                .pathMatchers(HttpMethod.POST, "/products/*/suppliers/*/prices").hasAuthority("SCOPE_price:create")
                                .pathMatchers(HttpMethod.PUT, "/products/*/suppliers/*/prices/*").hasAuthority("SCOPE_price:update")
                                .anyExchange().permitAll()
                )
                .anonymous()
                .and()
                .oauth2ResourceServer(ServerHttpSecurity.OAuth2ResourceServerSpec::jwt)
                .build();
    }
}

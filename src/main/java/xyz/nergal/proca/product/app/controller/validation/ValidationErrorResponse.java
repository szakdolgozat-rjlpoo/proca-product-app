package xyz.nergal.proca.product.app.controller.validation;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ValidationErrorResponse {

    private List<RequestError> requestErrors = new ArrayList<>();
}

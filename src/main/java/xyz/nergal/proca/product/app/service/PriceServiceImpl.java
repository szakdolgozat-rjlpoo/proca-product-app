package xyz.nergal.proca.product.app.service;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.PriceRepository;
import xyz.nergal.proca.product.app.domain.ProductRepository;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.service.convert.EntityConversionService;
import xyz.nergal.proca.product.app.service.event.PriceChangedEvent;

@Service
public class PriceServiceImpl implements PriceService {

    private final ProductRepository productRepository;

    private final PriceRepository priceRepository;

    private final EntityConversionService entityConversionService;

    private final ApplicationEventPublisher applicationEventPublisher;

    public PriceServiceImpl(
            ProductRepository productRepository,
            PriceRepository priceRepository,
            EntityConversionService entityConversionService,
            ApplicationEventPublisher applicationEventPublisher) {
        this.productRepository = productRepository;
        this.priceRepository = priceRepository;
        this.entityConversionService = entityConversionService;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public Mono<Page<PriceResponse>> list(Long productId, Pageable pageable) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .map(product -> priceRepository.findAllByProduct(product, pageable))
                .map(page -> page.map(price -> entityConversionService.convert(price, PriceResponse.class)));
    }

    @Override
    public Mono<PriceResponse> get(Long productId, Long id) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .flatMap(product -> Mono.justOrEmpty(priceRepository.findByProductAndId(product, id)))
                .flatMap(price -> Mono.justOrEmpty(entityConversionService.convert(price, PriceResponse.class)));
    }

    @Override
    public Mono<Page<PriceResponse>> list(Long productId, String supplierId, Pageable pageable) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .map(product -> priceRepository.findAllByProductAndSupplier(product, supplierId, pageable))
                .map(page -> page.map(price -> entityConversionService.convert(price, PriceResponse.class)));
    }

    @Override
    public Mono<PriceResponse> get(Long productId, String supplierId, Long id) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .flatMap(product -> Mono.justOrEmpty(priceRepository.findByProductAndSupplierAndId(product, supplierId, id)))
                .flatMap(price -> Mono.justOrEmpty(entityConversionService.convert(price, PriceResponse.class)));
    }

    @Override
    public Mono<PriceResponse> store(Long productId, String supplierId, PriceRequest request) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .zipWith(Mono.justOrEmpty(entityConversionService.convert(request, Price.class)))
                .map(objects -> {
                    objects.getT2().setProduct(objects.getT1());
                    objects.getT2().setSupplier(supplierId);
                    return objects.getT2();
                })
                .map(priceRepository::save)
                .doOnNext(price -> applicationEventPublisher.publishEvent(new PriceChangedEvent(price)))
                .flatMap(price -> Mono.justOrEmpty(entityConversionService.convert(price, PriceResponse.class)));
    }

    @Override
    public Mono<PriceResponse> update(Long productId, String supplierId, Long id, PriceRequest request) {
        return Mono.justOrEmpty(productRepository.findByIdAndDeletedAtIsNull(productId))
                .flatMap(product -> Mono.justOrEmpty(priceRepository.findByProductAndSupplierAndId(product, supplierId, id)))
                .zipWith(Mono.justOrEmpty(entityConversionService.convert(request, Price.class)))
                .map(objects -> {
                    objects.getT2().setId(objects.getT1().getId());
                    objects.getT2().setProduct(objects.getT1().getProduct());
                    objects.getT2().setSupplier(objects.getT1().getSupplier());
                    objects.getT2().setCreatedAt(objects.getT1().getCreatedAt());
                    objects.getT2().setUpdatedAt(objects.getT1().getUpdatedAt());
                    return objects.getT2();
                })
                .map(priceRepository::save)
                .doOnNext(price -> applicationEventPublisher.publishEvent(new PriceChangedEvent(price)))
                .flatMap(price -> Mono.justOrEmpty(entityConversionService.convert(price, PriceResponse.class)));
    }
}

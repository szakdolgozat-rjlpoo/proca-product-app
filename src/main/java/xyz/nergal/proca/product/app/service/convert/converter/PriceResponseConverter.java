package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.entity.Price;

public class PriceResponseConverter implements Converter<Price, PriceResponse> {

    @Override
    public PriceResponse convert(@Nullable Price source) {
        if (source == null) {
            return null;
        }

        return PriceResponse.builder()
                .id(source.getId())
                .currencyCode(source.getCurrencyCode())
                .value(source.getValue())
                .supplier(source.getSupplier())
                .updatedAt(source.getUpdatedAt())
                .build();
    }
}

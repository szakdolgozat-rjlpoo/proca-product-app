package xyz.nergal.proca.product.app.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.util.Optional;

public interface PriceRepository extends CrudRepository<Price, Long> {

    /**
     * Returns a {@link Page} of entities owned by a specific {@link Product} meeting the paging restriction provided
     * in the {@code Pageable} object.
     *
     * @param pageable paging restriction.
     * @return a page of entities.
     */
    Page<Price> findAllByProduct(Product product, Pageable pageable);

    /**
     * Returns a {@link Page} of entities owned by a specific {@link Product} and supplier meeting the paging
     * restriction provided in the {@code Pageable} object.
     *
     * @param pageable paging restriction.
     * @return a page of entities.
     */
    Page<Price> findAllByProductAndSupplier(Product product, String supplier, Pageable pageable);

    /**
     * Retrieves an entity by its {@link Product} and id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given product and id or {@literal Optional#empty()} if none found.
     */
    Optional<Price> findByProductAndId(Product product, Long id);

    /**
     * Retrieves an entity by its {@link Product}, supplier and id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given product, supplier and id or {@literal Optional#empty()} if none found.
     */
    Optional<Price> findByProductAndSupplierAndId(Product product, String supplier, Long id);
}

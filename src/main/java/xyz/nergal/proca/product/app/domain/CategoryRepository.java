package xyz.nergal.proca.product.app.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.nergal.proca.product.app.domain.entity.Category;

import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    /**
     * Retrieves an entity by its id.
     *
     * @param name must not be {@literal null}.
     * @return the entity with the given name or {@literal Optional#empty()} if none found.
     */
    Optional<Category> findByName(String name);
}

package xyz.nergal.proca.product.app.domain.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Product.
 *
 * A product belongs to many category. (N to N)
 * A product has many prices. (1 to N)
 */
@Entity
@Table(name = "products")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    /**
     * Identifier of a product.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Name of a product.
     */
    @Column(nullable = false)
    private String name;

    /**
     * Image of a product.
     */
    private String img;

    /**
     * Description of a product.
     */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    /**
     * Creation time of a product.
     */
    @CreationTimestamp
    private LocalDateTime createdAt;

    /**
     * Update time of a product.
     */
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    /**
     * Delete time of a product.
     */
    private LocalDateTime deletedAt;

    /**
     * Prices which the product has.
     */
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private List<Price> prices;

    /**
     * Categories which the product belongs to.
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "category_product",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> categories;
}

package xyz.nergal.proca.product.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.ReactivePageableHandlerMethodArgumentResolver;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;

/**
 * Spring Boot WebFlux Starter 2.2.0 does not contain auto configuration for resolving
 * {@link Pageable}. This configuration adds {@link ReactivePageableHandlerMethodArgumentResolver}
 * as a {@link ReactivePageableConfiguration}.
 */
@Configuration
public class ReactivePageableConfiguration {

    @Bean
    public HandlerMethodArgumentResolver reactivePageableHandlerMethodArgumentResolver() {
        return new ReactivePageableHandlerMethodArgumentResolver();
    }
}

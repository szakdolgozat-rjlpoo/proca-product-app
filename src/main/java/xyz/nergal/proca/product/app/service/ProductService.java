package xyz.nergal.proca.product.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;

public interface ProductService {

    /**
     * Return a list of products for the given page.
     *
     * @param pageable The pageable information.
     * @return List of products.
     */
    Mono<Page<ProductResponse>> list(Pageable pageable);

    /**
     * Return a product matching the given id.
     *
     * @param id the id of the expected product.
     * @return a product matching the given id.
     */
    Mono<ProductResponse> get(Long id);

    /**
     * Store a product.
     *
     * @param request The product details.
     * @return The stored product.
     */
    Mono<ProductResponse> store(ProductRequest request);

    /**
     * Update a product.
     *
     * @param id the id of the expected product.
     * @param request The product details.
     * @return The updated product.
     */
    Mono<ProductResponse> update(Long id, ProductRequest request);

    /**
     * Delete a product.
     *
     * @param id the id of the expected product.
     * @return The deleted product
     */
    Mono<ProductResponse> delete(Long id);
}

package xyz.nergal.proca.product.app.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Category of products.
 *
 * A category belongs to many product. (N to N)
 */
@Entity
@Table(name = "categories")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    /**
     * Identifier of a category.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Name of a category.
     */
    private String name;

    /**
     * Creation time of a category.
     */
    @CreationTimestamp
    private LocalDateTime createdAt;

    /**
     * Update time of a category.
     */
    @UpdateTimestamp
    private LocalDateTime updatedAt;
}

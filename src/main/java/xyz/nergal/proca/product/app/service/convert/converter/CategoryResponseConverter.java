package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.app.domain.entity.Category;

public class CategoryResponseConverter implements Converter<Category, String> {

    @Override
    public String convert(@Nullable Category source) {
        if (source == null) {
            return null;
        }

        return source.getName();
    }
}

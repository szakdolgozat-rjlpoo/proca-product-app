package xyz.nergal.proca.product.app.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import xyz.nergal.proca.product.app.domain.CategoryRepository;
import xyz.nergal.proca.product.app.domain.entity.Category;

import java.util.Optional;

@Component
public class CategoryValidator implements Validator {

    private final CategoryRepository categoryRepository;

    public CategoryValidator(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return String.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        String categoryName = (String) target;

        Optional<Category> category = categoryRepository.findByName(categoryName);

        if (!category.isPresent()) {
            errors.rejectValue(null, "category.notFound", "Category not found");
        }
    }
}

package xyz.nergal.proca.product.app.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;
import xyz.nergal.proca.product.app.controller.validation.RequestError;
import xyz.nergal.proca.product.app.controller.validation.ValidationErrorResponse;

@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    @ExceptionHandler(WebExchangeBindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ValidationErrorResponse onWebExchangeBindException(WebExchangeBindException exception) {
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
            errorResponse.getRequestErrors().add(new RequestError(fieldError.getField(), fieldError.getDefaultMessage()));
        }
        return errorResponse;
    }
}

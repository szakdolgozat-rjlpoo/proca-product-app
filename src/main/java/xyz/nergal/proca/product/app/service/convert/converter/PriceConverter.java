package xyz.nergal.proca.product.app.service.convert.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.app.domain.entity.Price;

public class PriceConverter implements Converter<PriceRequest, Price> {

    @Override
    public Price convert(@Nullable PriceRequest source) {
        if (source == null) {
            return null;
        }

        return Price.builder()
                .currencyCode(source.getCurrencyCode())
                .value(source.getValue())
                .build();
    }
}

package xyz.nergal.proca.product.app.controller.validation;

import lombok.Getter;

@Getter
public class RequestError {

    private final String fieldName;

    private final String message;

    public RequestError(String fieldName, String message) {
        this.fieldName = fieldName;
        this.message = message;
    }
}

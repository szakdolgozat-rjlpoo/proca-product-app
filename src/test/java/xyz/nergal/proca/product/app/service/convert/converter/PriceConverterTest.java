package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.app.domain.entity.Price;

import java.math.BigDecimal;

class PriceConverterTest {

    @Test
    void convert() {
        PriceRequest priceRequest = PriceRequest.builder()
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .build();

        PriceConverter converter = new PriceConverter();

        Price actualPrice = converter.convert(priceRequest);

        Price expectedPrice = Price.builder()
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .build();

        Assert.assertEquals(expectedPrice, actualPrice);
    }
}

package xyz.nergal.proca.product.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ProductServerApplicationTests {

    @Test
    void contextLoads() {
        // Intentionally Left Blank
        Assertions.assertTrue(true);
    }

}

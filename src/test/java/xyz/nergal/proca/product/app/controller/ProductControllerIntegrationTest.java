package xyz.nergal.proca.product.app.controller;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.transaction.annotation.Transactional;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.JwtAuthTokenProperties;
import xyz.nergal.proca.product.app.config.ReactiveOAuth2ResourceServerJwkConfiguration;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableConfigurationProperties(JwtAuthTokenProperties.class)
@Transactional
@SuppressWarnings("squid:S1192")
class ProductControllerIntegrationTest {

    @Autowired
    private JwtAuthTokenProperties jwtAuthTokenProperties;

    @Autowired
    private WebTestClient webTestClient;

    private static ClientAndServer mockServer;

    @BeforeAll
    public static void setUpBeforeClass() {
        mockServer = ClientAndServer.startClientAndServer(9999);

        HttpRequest request = HttpRequest.request("/.well-known/jwks.json").withMethod(HttpMethod.GET.name());
        String responseBody = JwtAuthTokenProperties.getJsonWebKeySet();
        HttpResponse response = HttpResponse.response(responseBody)
                .withStatusCode(HttpStatus.OK.value())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        mockServer.when(request)
                .respond(response);

        ReactiveOAuth2ResourceServerJwkConfiguration.setJwtTimestampValidatorClock(
                Clock.fixed(Instant.parse("2019-10-14T01:00:00Z"), ZoneOffset.UTC)
        );
    }

    @AfterAll
    public static void tearDownAfterClass() {
        mockServer.stop();

        ReactiveOAuth2ResourceServerJwkConfiguration.setJwtTimestampValidatorClock(Clock.systemUTC());
    }

    @Test
    void indexShouldRespondOk() {
        webTestClient.get()
                .uri("/products")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    void readShouldRespondNotFoundWhenProductDoesNotExist() {
        webTestClient.get()
                .uri("/products/9999")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOkWhenProductExists() {
        webTestClient.get()
                .uri("/products/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(ProductResponse.class)
                .consumeWith(result -> {
                    Assert.assertNotNull(result.getResponseBody());
                    Assert.assertEquals(Long.valueOf(1), result.getResponseBody().getId());
                });
    }

    @Test
    void createShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        webTestClient.post()
                .uri("/products")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        webTestClient.post()
                .uri("/products")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getNoScope()))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondBadRequestWhenClientSendsInvalidPayload() {
        ProductRequest request = ProductRequest.builder()
                .build();

        webTestClient.post()
                .uri("/products")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductCreateScope()))
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void createShouldRespondBadRequestWhenPayloadContainsInvalidCategory() {
        ProductRequest request = ProductRequest.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        webTestClient.post()
                .uri("/products")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductCreateScope()))
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void createShouldRespondCreatedWhenClientSendsCorrectBearerAndCorrectPayload() {
        ProductRequest request = ProductRequest.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Collections.singletonList("Camera")))
                .build();

        webTestClient.post()
                .uri("/products")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductCreateScope()))
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(request)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(ProductResponse.class)
                .consumeWith(result -> {
                    Assert.assertNotNull(result.getResponseBody());
                    Assert.assertNotNull(result.getResponseBody().getId());
                });
    }

    @Test
    void updateShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        webTestClient.put()
                .uri("/products/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        webTestClient.put()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getNoScope()))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondBadRequestWhenClientSendsInvalidPayload() {
        ProductRequest request = ProductRequest.builder()
                .build();

        webTestClient.put()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductUpdateScope()))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void updateShouldRespondBadRequestWhenPayloadContainsInvalidCategory() {
        ProductRequest request = ProductRequest.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        webTestClient.put()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductUpdateScope()))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void updateShouldRespondNotFoundWhenProductDoesNotExist() {
        ProductRequest request = ProductRequest.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Collections.singletonList("Camera")))
                .build();

        webTestClient.put()
                .uri("/products/9999")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductUpdateScope()))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void updateShouldRespondOkWhenClientSendsCorrectBearerAndCorrectPayload() {
        ProductRequest request = ProductRequest.builder()
                .name("newName")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Collections.singletonList("Camera")))
                .build();

        webTestClient.put()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductUpdateScope()))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(ProductResponse.class)
                .consumeWith(result -> {
                    Assert.assertNotNull(result.getResponseBody());
                    Assert.assertEquals(Long.valueOf(1), result.getResponseBody().getId());
                    Assert.assertEquals("newName", result.getResponseBody().getName());
                });
    }

    @Test
    void deleteShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        webTestClient.delete()
                .uri("/products/1")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void deleteShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        webTestClient.delete()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getNoScope()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void deleteShouldRespondNotFoundWhenProductDoesNotExist() {
        webTestClient.delete()
                .uri("/products/9999")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductDeleteScope()))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void deleteShouldRespondNoContentWhenClientSendsCorrectBearer() {
        webTestClient.delete()
                .uri("/products/1")
                .headers(httpHeaders -> httpHeaders.setBearerAuth(jwtAuthTokenProperties.getProductDeleteScope()))
                .exchange()
                .expectStatus().isNoContent();
    }
}

package xyz.nergal.proca.product.app.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.domain.CategoryRepository;
import xyz.nergal.proca.product.app.domain.ProductRepository;
import xyz.nergal.proca.product.app.domain.entity.Product;
import xyz.nergal.proca.product.app.service.convert.EntityConversionService;
import xyz.nergal.proca.product.app.service.exception.CategoryNotFoundException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@Import({
        ProductServiceImpl.class,
        EntityConversionService.class,
})
class ProductServiceImplTest {

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Test
    void list() {
        PageRequest pageable = PageRequest.of(0, 20);

        List<Product> products = Collections.singletonList(Product.builder().build());
        Page<Product> page = new PageImpl<>(products, pageable, products.size());
        Mockito.when(productRepository.findAllByDeletedAtIsNull(ArgumentMatchers.eq(pageable)))
                .thenReturn(page);

        List<ProductResponse> expectedResponses = Collections.singletonList(
                ProductResponse.builder().categories(Collections.emptySet()).prices(Collections.emptyList()).build()
        );
        PageImpl<ProductResponse> expectedPage = new PageImpl<>(expectedResponses, pageable, expectedResponses.size());

        StepVerifier.create(productService.list(pageable))
                .expectNext(expectedPage)
                .verifyComplete();
    }

    @Test
    void getShouldReturnEmptyWhenProductDoesNotExist() {
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(Optional.empty());

        StepVerifier.create(productService.get(id))
                .verifyComplete();
    }

    @Test
    void get() {
        long id = 1L;

        Optional<Product> product = Optional.of(Product.builder().build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(product);

        ProductResponse expectedResponse = ProductResponse.builder()
                .categories(Collections.emptySet())
                .prices(Collections.emptyList())
                .build();

        StepVerifier.create(productService.get(id))
                .expectNext(expectedResponse)
                .verifyComplete();
    }

    @Test
    void storeShouldReturnErrorWhenCategoryDoesNotExist() {
        Mockito.when(categoryRepository.findByName(ArgumentMatchers.any()))
                .thenReturn(Optional.empty());

        StepVerifier.create(productService.store(ProductRequest.builder().categories(Collections.singleton("")).build()))
                .expectError(CategoryNotFoundException.class)
                .verify();
    }

    @Test
    void store() {
        Mockito.when(productRepository.save(ArgumentMatchers.any()))
                .thenAnswer(i -> i.getArguments()[0]);

        ProductResponse expectedResponse = ProductResponse.builder()
                .categories(Collections.emptySet())
                .prices(Collections.emptyList())
                .build();

        StepVerifier.create(productService.store(ProductRequest.builder().build()))
                .expectNext(expectedResponse)
                .verifyComplete();
    }

    @Test
    void updateShouldReturnEmptyWhenProductDoesNotExist() {
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(Optional.empty());

        StepVerifier.create(productService.update(id, ProductRequest.builder().build()))
                .verifyComplete();
    }

    @Test
    void updateShouldReturnErrorWhenCategoryDoesNotExist() {
        long id = 1L;

        Optional<Product> product = Optional.of(Product.builder().build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(product);

        Mockito.when(categoryRepository.findByName(ArgumentMatchers.any()))
                .thenReturn(Optional.empty());

        StepVerifier.create(productService.update(id, ProductRequest.builder().categories(Collections.singleton("")).build()))
                .expectError(CategoryNotFoundException.class)
                .verify();
    }

    @Test
    void update() {
        long id = 1L;

        Optional<Product> product = Optional.of(Product.builder()
                .id(id)
                .createdAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(product);

        Mockito.when(productRepository.save(ArgumentMatchers.any()))
                .thenAnswer(i -> i.getArguments()[0]);

        ProductResponse expectedResponse = ProductResponse.builder()
                .id(id)
                .name("name")
                .categories(Collections.emptySet())
                .prices(Collections.emptyList())
                .build();

        StepVerifier.create(productService.update(id, ProductRequest.builder().name("name").build()))
                .expectNext(expectedResponse)
                .verifyComplete();

        Mockito.verify(productRepository).save(ArgumentMatchers.eq(Product.builder()
                .id(id)
                .name("name")
                .categories(Collections.emptySet())
                .createdAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build()));
    }

    @Test
    void deleteShouldReturnEmptyWhenProductDoesNotExist() {
        long id = 1L;

        Optional<Product> product = Optional.empty();
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(product);

        StepVerifier.create(productService.delete(id))
                .verifyComplete();
    }

    @Test
    void delete() {
        long id = 1L;

        Optional<Product> product = Optional.of(Product.builder().id(id).build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(id)))
                .thenReturn(product);

        Mockito.when(productRepository.save(ArgumentMatchers.any()))
                .thenAnswer(i -> i.getArguments()[0]);

        ProductResponse expectedResponse = ProductResponse.builder()
                .id(id)
                .categories(Collections.emptySet())
                .prices(Collections.emptyList())
                .build();

        StepVerifier.create(productService.delete(id))
                .expectNext(expectedResponse)
                .verifyComplete();

        Mockito.verify(productRepository).save(ArgumentMatchers.argThat(argument -> argument != null
                && Long.valueOf(1).equals(argument.getId())
                && argument.getDeletedAt() != null));
    }
}

package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.domain.entity.Category;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class ProductResponseConverterTest {

    @Test
    void convert() {
        Set<Category> categories = new HashSet<>(Collections.singletonList(
                Category.builder().id(1L).name("name").build()
        ));

        List<Price> prices = Collections.singletonList(
                Price.builder().id(1L).currencyCode("USD").value(new BigDecimal("1.1")).supplier("object-id").build()
        );

        Product product = Product.builder()
                .id(1L)
                .name("name")
                .description("description")
                .img("img")
                .categories(categories)
                .prices(prices)
                .build();

        ProductResponseConverter converter = new ProductResponseConverter(
                new CategoryResponseConverter(), new PriceResponseConverter());

        ProductResponse actualResponse = converter.convert(product);

        HashSet<String> expectedCategories = new HashSet<>(Collections.singleton("name"));

        List<PriceResponse> expectedPrices = Collections.singletonList(
                PriceResponse.builder()
                        .id(1L)
                        .currencyCode("USD")
                        .value(new BigDecimal("1.1"))
                        .supplier("object-id")
                        .build()
        );

        ProductResponse expectedResponse = ProductResponse.builder()
                .id(1L)
                .name("name")
                .description("description")
                .img("img")
                .categories(expectedCategories)
                .prices(expectedPrices)
                .build();

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}

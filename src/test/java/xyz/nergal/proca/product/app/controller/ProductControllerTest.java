package xyz.nergal.proca.product.app.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.common.io.ResourceUtils;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.config.PagingJsonSerializationConfiguration;
import xyz.nergal.proca.product.app.config.ReactivePageableConfiguration;
import xyz.nergal.proca.product.app.config.SecurityConfiguration;
import xyz.nergal.proca.product.app.controller.validation.ProductRequestValidator;
import xyz.nergal.proca.product.app.service.ProductService;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebFluxTest({ProductController.class})
@Import({
        PagingJsonSerializationConfiguration.class,
        SecurityConfiguration.class,
        ReactivePageableConfiguration.class,
})
@SuppressWarnings("squid:S1192")
class ProductControllerTest {

    @MockBean
    private ProductRequestValidator productRequestValidator;

    @MockBean
    private ProductService productService;

    @MockBean
    private ReactiveJwtDecoder jwtDecoder;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private WebTestClient client;

    @BeforeEach
    void setUp() {
        Mockito.when(productRequestValidator.supports(ArgumentMatchers.any())).thenReturn(true);
    }

    @Test
    void indexShouldRespondOk() {
        Mockito.when(productService.list(ArgumentMatchers.any()))
                .thenReturn(Mono.just(Page.empty()));

        this.client.get()
                .uri("/products")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void indexShouldRespondWithCorrectPayload() throws IOException {
        PriceResponse priceResponse = PriceResponse.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        ProductResponse productResponse = ProductResponse.builder()
                .id(1L)
                .name("name")
                .img("img")
                .description("description")
                .prices(Collections.singletonList(priceResponse))
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        List<ProductResponse> productResponses = Collections.singletonList(productResponse);
        Pageable pageable = PageRequest.of(0, 20, Sort.by("id"));
        Page<ProductResponse> page = new PageImpl<>(productResponses, pageable, productResponses.size());

        Mockito.when(productService.list(ArgumentMatchers.any()))
                .thenReturn(Mono.just(page));

        String expectedJson = ResourceUtils.asString(resourceLoader.getResource("classpath:static/product-index-payload.json"));

        this.client.get()
                .uri("/products")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(expectedJson);
    }

    @Test
    void readShouldRespondNotFound() {
        Mockito.when(productService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        this.client.get()
                .uri("/products/1")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOk() {
        Mockito.when(productService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.just(ProductResponse.builder().build()));

        this.client.get()
                .uri("/products/1")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void readShouldRespondWithCorrectPayload() throws IOException {
        PriceResponse priceResponse = PriceResponse.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        ProductResponse productResponse = ProductResponse.builder()
                .id(1L)
                .name("name")
                .img("img")
                .description("description")
                .prices(Collections.singletonList(priceResponse))
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        Mockito.when(productService.get(ArgumentMatchers.any()))
                .thenReturn(Mono.just(productResponse));

        String expectedJson = ResourceUtils.asString(resourceLoader.getResource("classpath:static/product-read-payload.json"));

        this.client.get()
                .uri("/products/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(expectedJson);
    }

    @Test
    void createShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        this.client.post()
                .uri("/products")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        Jwt jwt = jwt().claim("scope", "").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.post()
                .uri("/products")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondBadRequestWhenPayloadInvalid() {
        Jwt jwt = jwt().claim("scope", "product:create").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        ProductRequest productRequest = ProductRequest.builder().build();

        this.client.post()
                .uri("/products")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void createShouldRespondCreatedWhenClientSendsCorrectBearerAndCorrectPayload() {
        Mockito.when(this.productService.store(ArgumentMatchers.any()))
                .thenReturn(Mono.just(ProductResponse.builder().build()));

        Jwt jwt = jwt().claim("scope", "product:create").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        ProductRequest productRequest = ProductRequest.builder()
                .name("name")
                .img("img")
                .description("description")
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        this.client.post()
                .uri("/products")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    void updateShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        this.client.put()
                .uri("/products/1")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondForbiddenWhenWhenClientSendsCorrectBearerAndCorrectPayload() {
        Jwt jwt = jwt().claim("scope", "").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.put()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondBadRequestWhenPayloadInvalid() {
        Jwt jwt = jwt().claim("scope", "product:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        ProductRequest productRequest = ProductRequest.builder().build();

        this.client.put()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void updateShouldRespondNotFoundWhenProductServiceReturnsEmptyMono() {
        Mockito.when(this.productService.update(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        Jwt jwt = jwt().claim("scope", "product:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        ProductRequest productRequest = ProductRequest.builder()
                .name("name")
                .img("img")
                .description("description")
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        this.client.put()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void updateShouldRespondOkWhenClientSendsCorrectBearerAndCorrectPayload() {
        Mockito.when(this.productService.update(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(ProductResponse.builder().build()));

        Jwt jwt = jwt().claim("scope", "product:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        ProductRequest productRequest = ProductRequest.builder()
                .name("name")
                .img("img")
                .description("description")
                .categories(new HashSet<>(Arrays.asList("category1", "category2")))
                .build();

        this.client.put()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void deleteShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        this.client.delete()
                .uri("/products/1")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void deleteShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        Jwt jwt = jwt().claim("scope", "").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.delete()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void deleteShouldRespondNotFoundWhenProductServiceReturnsEmptyMono() {
        Mockito.when(productService.delete(ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        Jwt jwt = jwt().claim("scope", "product:delete").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.delete()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void deleteShouldRespondNoContentWhenClientSendsCorrectBearer() {
        Mockito.when(productService.delete(ArgumentMatchers.any()))
                .thenReturn(Mono.just(ProductResponse.builder().build()));

        Jwt jwt = jwt().claim("scope", "product:delete").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.delete()
                .uri("/products/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isNoContent();
    }

    private Jwt.Builder jwt() {
        return Jwt.withTokenValue("token").header("alg", "none");
    }
}

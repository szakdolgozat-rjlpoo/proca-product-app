package xyz.nergal.proca.product.app.service.event;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.product.api.PriceChangeResponse;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.time.Duration;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PriceChangedEventRedisPublisherIntegrationTest {

    @Autowired
    private PriceChangedEventRedisPublisher priceChangedEventRedisPublisher;

    @Autowired
    private ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    @Test
    void onApplicationEvent() {
        String channel = "price-change";

        Price source = Price.builder()
                .id(1L)
                .product(Product.builder().id(2L).build())
                .build();
        PriceChangedEvent event = new PriceChangedEvent(source);

        StepVerifier.create(reactiveRedisTemplate.listenToChannel(channel))
                .thenAwait(Duration.ofMillis(500))
                .then(() -> priceChangedEventRedisPublisher.onApplicationEvent(event))
                .assertNext(message -> {
                    PriceChangeResponse response = message.getMessage();
                    Assertions.assertEquals(2, response.getProductId());
                    Assertions.assertEquals(1, response.getPriceResponse().getId());
                })
                .thenAwait(Duration.ofMillis(10))
                .thenCancel()
                .verify(Duration.ofSeconds(3));
    }
}

package xyz.nergal.proca.product.app;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.ManagedBean;

/**
 * JWT authentication properties.
 */
@ManagedBean
@ConfigurationProperties(prefix = "app.jwt.token")
public class JwtAuthTokenProperties {

    private String noScope;

    private String productCreateScope;

    private String productUpdateScope;

    private String productDeleteScope;

    private String priceCreateScope;

    private String priceUpdateScope;

    public static String getJsonWebKeySet() {
        return "{\"keys\":[{\"alg\":\"RS256\",\"kty\":\"RSA\",\"use\":\"sig\",\"x5c\":[\"MIIDDTCCAfWgAwIBAgIJcGKPM6j" +
                "uvlptMA0GCSqGSIb3DQEBCwUAMCQxIjAgBgNVBAMTGWRldi00dm41a2J2bi5ldS5hdXRoMC5jb20wHhcNMTkxMDEwMTgyMzA3Wh" +
                "cNMzMwNjE4MTgyMzA3WjAkMSIwIAYDVQQDExlkZXYtNHZuNWtidm4uZXUuYXV0aDAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCA" +
                "Q8AMIIBCgKCAQEAyhkbkXGoYupAoaU3wKlZ7CRfXidazjJXykgAuekri02oBqhke9aIut5WyVb6Q0vmbyMWXelXpu8/jZLiD1Ym" +
                "JftFochvji89A3kFf5sEL5xhdEWDxuV5w01YRXbPpenW9hx2Alrr6gHPsmQh69g5YaOcOXtaJu7/1EdlLwFTpZk0Alo77g9QuZM" +
                "4oL8nr+jtyBq/FU1ioyD9YnDUojSP1CbNvi61OEr7fwafKi4sZ1gpxk+lSkZMRDo+VUgBNG2ev2ySQ8Y6GDpVuPdD3xumYiKd2W" +
                "z/t6xW79T/0wrVvdfy935GVpuE/D3jSMZ9YcaTt+BoIijjWxOmbrk/dNm+mQIDAQABo0IwQDAPBgNVHRMBAf8EBTADAQH/MB0GA" +
                "1UdDgQWBBQ1zOguAD56EIR9g2InxPpUxFiSHzAOBgNVHQ8BAf8EBAMCAoQwDQYJKoZIhvcNAQELBQADggEBAHt/31CZw/zBbBkb" +
                "qwbMogGh0ml5r9iwExfzg2KDUS0wKmniQ9WiKrXpG6kFd5HJzqIIDbzu6dCN1/edce1uSLoFCd+l81263DfEUuaKnzJNrpmCzNK" +
                "OgSQzVd5z1fl7v8xmBziSEhsQroQgnhTAtxLHSUDFzMaHr7VifXbp75BiSBLs+zcrjZvilSis2q3P91/rU/4h2CVQrW7RqS/cya" +
                "70FEGPhn58Sish6ILybKy5qfG6ZONLAV7DNKUamwUidkMzlyHr2NRqG3K+6djZr+pbfQ817q/IMijjaVi1uGce1dQ3Yr8KGEERC" +
                "Eq/I+pa6BMs9kic/VZFEsu07N2E+tk=\"],\"n\":\"yhkbkXGoYupAoaU3wKlZ7CRfXidazjJXykgAuekri02oBqhke9aIut5W" +
                "yVb6Q0vmbyMWXelXpu8_jZLiD1YmJftFochvji89A3kFf5sEL5xhdEWDxuV5w01YRXbPpenW9hx2Alrr6gHPsmQh69g5YaOcOXt" +
                "aJu7_1EdlLwFTpZk0Alo77g9QuZM4oL8nr-jtyBq_FU1ioyD9YnDUojSP1CbNvi61OEr7fwafKi4sZ1gpxk-lSkZMRDo-VUgBNG" +
                "2ev2ySQ8Y6GDpVuPdD3xumYiKd2Wz_t6xW79T_0wrVvdfy935GVpuE_D3jSMZ9YcaTt-BoIijjWxOmbrk_dNm-mQ\",\"e\":\"" +
                "AQAB\",\"kid\":\"QTZDRTBBOEVCNzExNUUwOUQ2ODI0NTk2QTZCRDY1NzA1RDRFQzBBOA\",\"x5t\":\"QTZDRTBBOEVCNzE" +
                "xNUUwOUQ2ODI0NTk2QTZCRDY1NzA1RDRFQzBBOA\"}]}";
    }

    public String getNoScope() {
        return noScope;
    }

    public void setNoScope(String noScope) {
        this.noScope = noScope;
    }

    public String getProductCreateScope() {
        return productCreateScope;
    }

    public void setProductCreateScope(String productCreateScope) {
        this.productCreateScope = productCreateScope;
    }

    public String getProductUpdateScope() {
        return productUpdateScope;
    }

    public void setProductUpdateScope(String productUpdateScope) {
        this.productUpdateScope = productUpdateScope;
    }

    public String getProductDeleteScope() {
        return productDeleteScope;
    }

    public void setProductDeleteScope(String productDeleteScope) {
        this.productDeleteScope = productDeleteScope;
    }

    public String getPriceCreateScope() {
        return priceCreateScope;
    }

    public void setPriceCreateScope(String priceCreateScope) {
        this.priceCreateScope = priceCreateScope;
    }

    public String getPriceUpdateScope() {
        return priceUpdateScope;
    }

    public void setPriceUpdateScope(String priceUpdateScope) {
        this.priceUpdateScope = priceUpdateScope;
    }
}

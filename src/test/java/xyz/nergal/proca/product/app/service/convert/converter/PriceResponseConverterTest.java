package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.entity.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;

class PriceResponseConverterTest {

    @Test
    void convert() {
        Price price = Price.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        PriceResponseConverter converter = new PriceResponseConverter();

        PriceResponse actualResponse = converter.convert(price);

        PriceResponse expectedResponse = PriceResponse.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}

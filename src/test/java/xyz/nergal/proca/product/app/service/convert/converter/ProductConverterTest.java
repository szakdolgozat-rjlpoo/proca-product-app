package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.app.domain.entity.Category;
import xyz.nergal.proca.product.app.domain.entity.Product;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class ProductConverterTest {

    @Test
    void convert() {
        ProductRequest productRequest = ProductRequest.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(new HashSet<>(Collections.singleton("name")))
                .build();

        ProductConverter converter = new ProductConverter(new CategoryConverter());

        Product actualProduct = converter.convert(productRequest);

        Set<Category> expectedCategories = new HashSet<>(Collections.singleton(
                Category.builder().name("name").build()
        ));

        Product expectedProduct = Product.builder()
                .name("name")
                .description("description")
                .img("img")
                .categories(expectedCategories)
                .build();

        Assert.assertEquals(expectedProduct, actualProduct);
    }
}

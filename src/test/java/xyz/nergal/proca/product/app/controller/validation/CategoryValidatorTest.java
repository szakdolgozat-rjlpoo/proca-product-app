package xyz.nergal.proca.product.app.controller.validation;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import xyz.nergal.proca.product.app.domain.CategoryRepository;
import xyz.nergal.proca.product.app.domain.entity.Category;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class CategoryValidatorTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Test
    void supportsShouldFailWhenAnythingButStringClass() {
        CategoryValidator validator = new CategoryValidator(categoryRepository);

        Assert.assertFalse(validator.supports(Object.class));
    }

    @Test
    void supportsShouldSucceedWhenStringClass() {
        CategoryValidator validator = new CategoryValidator(categoryRepository);

        Assert.assertTrue(validator.supports(String.class));
    }

    @Test
    void validateShouldHaveErrorWhenCategoriesInvalid() {
        Mockito.when(categoryRepository.findByName(ArgumentMatchers.any()))
                .thenReturn(Optional.empty());

        String invalidCategory = "invalidCategory";

        CategoryValidator validator = new CategoryValidator(categoryRepository);

        Errors errors = new BeanPropertyBindingResult(invalidCategory, "invalidCategory");
        validator.validate(invalidCategory, errors);

        Assert.assertTrue(errors.hasErrors());
        Assert.assertNotNull(errors.getGlobalError());
    }

    @Test
    void validateShouldNotHaveErrorWhenCategoriesValid() {
        Mockito.when(categoryRepository.findByName(ArgumentMatchers.eq("validCategory")))
                .thenReturn(Optional.of(Category.builder().build()));

        String validCategory = "validCategory";

        CategoryValidator validator = new CategoryValidator(categoryRepository);

        Errors errors = new BeanPropertyBindingResult(validCategory, "validCategory");
        validator.validate(validCategory, errors);

        Assert.assertFalse(errors.hasErrors());
    }
}

package xyz.nergal.proca.product.app.controller.validation;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import xyz.nergal.proca.product.api.ProductRequest;

import java.util.Collections;

@ExtendWith(MockitoExtension.class)
class ProductRequestValidatorTest {

    @Spy
    @InjectMocks
    private CategoryValidator categoryValidator;

    @Test
    void supportsShouldFailWhenAnythingButStringClass() {
        ProductRequestValidator validator = new ProductRequestValidator(categoryValidator);

        Assert.assertFalse(validator.supports(Object.class));
    }

    @Test
    void supportsShouldSucceedWhenStringClass() {
        ProductRequestValidator validator = new ProductRequestValidator(categoryValidator);

        Assert.assertTrue(validator.supports(ProductRequest.class));
    }

    @Test
    void validateShouldCallCategoryValidatorWhenCategoriesNull() {
        ProductRequestValidator validator = new ProductRequestValidator(categoryValidator);

        ProductRequest productRequest = ProductRequest.builder()
                .build();

        Errors errors = new BeanPropertyBindingResult(productRequest, "productRequest");
        validator.validate(productRequest, errors);

        Mockito.verify(categoryValidator, Mockito.never()).validate(ArgumentMatchers.any(), ArgumentMatchers.eq(errors));
    }

    @Test
    void validateShouldCallCategoryValidatorWhenCategoriesEmpty() {
        ProductRequestValidator validator = new ProductRequestValidator(categoryValidator);

        ProductRequest productRequest = ProductRequest.builder()
                .categories(Collections.emptySet())
                .build();

        Errors errors = new BeanPropertyBindingResult(productRequest, "productRequest");
        validator.validate(productRequest, errors);

        Mockito.verify(categoryValidator, Mockito.never()).validate(ArgumentMatchers.any(), ArgumentMatchers.eq(errors));
    }

    @Test
    void validateShouldCallCategoryValidatorWhenCategoriesHasAny() {
        Mockito.doNothing().when(categoryValidator).validate(ArgumentMatchers.any(), ArgumentMatchers.any());

        ProductRequestValidator validator = new ProductRequestValidator(categoryValidator);

        String category = "category";
        ProductRequest productRequest = ProductRequest.builder()
                .categories(Collections.singleton(category))
                .build();

        Errors errors = new BeanPropertyBindingResult(productRequest, "productRequest");
        validator.validate(productRequest, errors);

        Mockito.verify(categoryValidator, Mockito.times(1)).validate(ArgumentMatchers.eq(category), ArgumentMatchers.eq(errors));
    }
}

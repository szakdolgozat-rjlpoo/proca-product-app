package xyz.nergal.proca.product.app.service;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.domain.PriceRepository;
import xyz.nergal.proca.product.app.domain.ProductRepository;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.domain.entity.Product;
import xyz.nergal.proca.product.app.service.convert.EntityConversionService;
import xyz.nergal.proca.product.app.service.event.PriceChangedEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@Import({
        PriceServiceImpl.class,
        EntityConversionService.class,
        SimpleApplicationEventMulticaster.class,
})
class PriceServiceImplTest {

    @Autowired
    private ApplicationEventMulticaster applicationEventMulticaster;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private PriceRepository priceRepository;

    @Autowired
    private PriceService priceService;

    private ApplicationListener<PriceChangedEvent> priceChangedEventApplicationListener;

    private List<PriceChangedEvent> priceChangedEventHistory = new ArrayList<>();

    /**
     * TODO: Remove when fix for [SPR-14335] is published
     * https://jira.spring.io/browse/SPR-14335?redirect=false
     * https://github.com/spring-projects/spring-framework/issues/18907
     */
    @BeforeEach
    void setUp() {
        priceChangedEventApplicationListener = event -> priceChangedEventHistory.add(event);

        applicationEventMulticaster.addApplicationListener(priceChangedEventApplicationListener);
    }

    @AfterEach
    void tearDown() {
        priceChangedEventHistory.clear();

        applicationEventMulticaster.removeApplicationListener(priceChangedEventApplicationListener);
    }

    @Test
    void listShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        PageRequest pageable = PageRequest.of(0, 20);

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.list(productId, pageable))
                .verifyComplete();
    }

    @Test
    void list() {
        long productId = 1L;
        PageRequest pageable = PageRequest.of(0, 20);

        Optional<Product> product = Optional.of(Product.builder().build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(product);
        List<Price> prices = Collections.singletonList(Price.builder().build());
        Page<Price> page = new PageImpl<>(prices, pageable, prices.size());
        Mockito.when(priceRepository.findAllByProduct(ArgumentMatchers.eq(product.get()), ArgumentMatchers.eq(pageable)))
                .thenReturn(page);

        List<PriceResponse> expectedResponses = Collections.singletonList(
                PriceResponse.builder().build()
        );
        PageImpl<PriceResponse> expectedPage = new PageImpl<>(expectedResponses, pageable, expectedResponses.size());

        StepVerifier.create(priceService.list(productId, pageable))
                .expectNext(expectedPage)
                .verifyComplete();
    }

    @Test
    void listFilteredBySupplierShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";
        PageRequest pageable = PageRequest.of(0, 20);

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.list(productId, supplierId, pageable))
                .verifyComplete();
    }

    @Test
    void listFilteredBySupplier() {
        long productId = 1L;
        String supplierId = "object-id";
        PageRequest pageable = PageRequest.of(0, 20);

        Optional<Product> product = Optional.of(Product.builder().build());
        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(product);
        List<Price> prices = Collections.singletonList(Price.builder().build());
        Page<Price> page = new PageImpl<>(prices, pageable, prices.size());
        Mockito.when(priceRepository.findAllByProductAndSupplier(ArgumentMatchers.eq(product.get()), ArgumentMatchers.eq(supplierId), ArgumentMatchers.eq(pageable)))
                .thenReturn(page);

        List<PriceResponse> expectedResponses = Collections.singletonList(
                PriceResponse.builder().build()
        );
        PageImpl<PriceResponse> expectedPage = new PageImpl<>(expectedResponses, pageable, expectedResponses.size());

        StepVerifier.create(priceService.list(productId, supplierId, pageable))
                .expectNext(expectedPage)
                .verifyComplete();
    }

    @Test
    void getShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.get(productId, id))
                .verifyComplete();
    }

    @Test
    void getShouldReturnEmptyWhenPriceDoesNotExist() {
        long productId = 1L;
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().build()));

        Mockito.when(priceRepository.findByProductAndId(ArgumentMatchers.any(), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.get(productId, id))
                .verifyComplete();
    }

    @Test
    void get() {
        long productId = 1L;
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().build()));

        Mockito.when(priceRepository.findByProductAndId(ArgumentMatchers.any(), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Price.builder().build()));

        StepVerifier.create(priceService.get(productId, id))
                .expectNext(PriceResponse.builder().build())
                .verifyComplete();
    }

    @Test
    void getFilteredBySupplierShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.get(productId, supplierId, id))
                .verifyComplete();
    }

    @Test
    void getFilteredBySupplierShouldReturnEmptyWhenPriceDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().build()));

        Mockito.when(priceRepository.findByProductAndSupplierAndId(
                ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.get(productId, supplierId, id))
                .verifyComplete();
    }

    @Test
    void getFilteredBySupplier() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().build()));

        Mockito.when(priceRepository.findByProductAndSupplierAndId(
                ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Price.builder().build()));

        StepVerifier.create(priceService.get(productId, supplierId, id))
                .expectNext(PriceResponse.builder().build())
                .verifyComplete();
    }

    @Test
    void storeShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.store(productId, supplierId, PriceRequest.builder().build()))
                .verifyComplete();
    }

    @Test
    void store() {
        long productId = 1L;
        String supplierId = "object-id";

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().id(productId).build()));

        Price expectedEntity = Price.builder().product(Product.builder().id(productId).build()).supplier(supplierId).build();
        Mockito.when(priceRepository.save(ArgumentMatchers.eq(expectedEntity)))
                .thenAnswer(i -> i.getArguments()[0]);

        StepVerifier.create(priceService.store(productId, supplierId, PriceRequest.builder().build()))
                .expectNext(PriceResponse.builder().supplier(supplierId).build())
                .verifyComplete();
    }

    @Test
    void storePublishesPriceChangedEvent() {
        long productId = 1L;
        String supplierId = "object-id";

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().id(productId).build()));

        Price expectedEntity = Price.builder().product(Product.builder().id(productId).build()).supplier(supplierId).build();
        Mockito.when(priceRepository.save(ArgumentMatchers.eq(expectedEntity)))
                .thenAnswer(i -> i.getArguments()[0]);

        StepVerifier.create(priceService.store(productId, supplierId, PriceRequest.builder().build()))
                .expectNextCount(1)
                .then(() -> {
                    Assert.assertEquals(1, priceChangedEventHistory.size());
                    Assert.assertEquals(expectedEntity, priceChangedEventHistory.get(0).getSource());
                })
                .verifyComplete();
    }

    @Test
    void updateShouldReturnEmptyWhenProductDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.update(productId, supplierId, id, PriceRequest.builder().build()))
                .verifyComplete();
    }

    @Test
    void updateShouldReturnEmptyWhenPriceDoesNotExist() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().build()));

        Mockito.when(priceRepository.findByProductAndSupplierAndId(
                ArgumentMatchers.eq(Product.builder().id(productId).build()), ArgumentMatchers.eq(supplierId), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.empty());

        StepVerifier.create(priceService.update(productId, supplierId, id, PriceRequest.builder().build()))
                .verifyComplete();
    }

    @Test
    void update() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().id(productId).build()));

        Mockito.when(priceRepository.findByProductAndSupplierAndId(
                ArgumentMatchers.eq(Product.builder().id(productId).build()), ArgumentMatchers.eq(supplierId), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Price.builder().product(Product.builder().id(productId).build()).build()));

        Price expectedEntity = Price.builder().product(Product.builder().id(productId).build()).build();
        Mockito.when(priceRepository.save(ArgumentMatchers.eq(expectedEntity)))
                .thenAnswer(i -> i.getArguments()[0]);

        StepVerifier.create(priceService.update(productId, supplierId, id, PriceRequest.builder().build()))
                .expectNext(PriceResponse.builder().build())
                .verifyComplete();
    }

    @Test
    void updatePublishesPriceChangedEvent() {
        long productId = 1L;
        String supplierId = "object-id";
        long id = 1L;

        Mockito.when(productRepository.findByIdAndDeletedAtIsNull(ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Product.builder().id(productId).build()));

        Mockito.when(priceRepository.findByProductAndSupplierAndId(
                ArgumentMatchers.eq(Product.builder().id(productId).build()), ArgumentMatchers.eq(supplierId), ArgumentMatchers.eq(productId)))
                .thenReturn(Optional.of(Price.builder().product(Product.builder().id(productId).build()).build()));

        Price expectedEntity = Price.builder().product(Product.builder().id(productId).build()).build();
        Mockito.when(priceRepository.save(ArgumentMatchers.eq(expectedEntity)))
                .thenAnswer(i -> i.getArguments()[0]);

        StepVerifier.create(priceService.update(productId, supplierId, id, PriceRequest.builder().build()))
                .expectNextCount(1)
                .then(() -> {
                    Assert.assertEquals(1, priceChangedEventHistory.size());
                    Assert.assertEquals(expectedEntity, priceChangedEventHistory.get(0).getSource());
                })
                .verifyComplete();
    }
}

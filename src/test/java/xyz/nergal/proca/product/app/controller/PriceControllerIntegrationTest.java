package xyz.nergal.proca.product.app.controller;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.transaction.annotation.Transactional;
import xyz.nergal.proca.product.api.PriceChangeResponse;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.JwtAuthTokenProperties;
import xyz.nergal.proca.product.app.config.ReactiveOAuth2ResourceServerJwkConfiguration;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableConfigurationProperties(JwtAuthTokenProperties.class)
@Transactional
@SuppressWarnings("squid:S1192")
class PriceControllerIntegrationTest {

    @Autowired
    private JwtAuthTokenProperties jwtAuthTokenProperties;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    private static ClientAndServer mockServer;

    @BeforeAll
    public static void setUpBeforeClass() {
        mockServer = ClientAndServer.startClientAndServer(9999);

        HttpRequest request = HttpRequest.request("/.well-known/jwks.json").withMethod(HttpMethod.GET.name());
        String responseBody = JwtAuthTokenProperties.getJsonWebKeySet();
        HttpResponse response = HttpResponse.response(responseBody)
                .withStatusCode(HttpStatus.OK.value())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        mockServer.when(request)
                .respond(response);

        ReactiveOAuth2ResourceServerJwkConfiguration.setJwtTimestampValidatorClock(
                Clock.fixed(Instant.parse("2019-10-14T01:00:00Z"), ZoneOffset.UTC)
        );
    }

    @AfterAll
    public static void tearDownAfterClass() {
        mockServer.stop();

        ReactiveOAuth2ResourceServerJwkConfiguration.setJwtTimestampValidatorClock(Clock.systemUTC());
    }

    @Test
    void indexShouldRespondNotFoundWhenProductDoesNotExist() {
        webTestClient.get()
                .uri("/products/9999/prices")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void indexShouldRespondOkWhenProductExists() {
        webTestClient.get()
                .uri("/products/1/prices")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    void readShouldRespondNotFoundWhenProductDoesNotExist() {
        webTestClient.get()
                .uri("/products/9999/prices/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondNotFoundWhenPriceDoesNotExist() {
        webTestClient.get()
                .uri("/products/1/prices/9999")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOkWhenProductAndPriceExist() {
        webTestClient.get()
                .uri("/products/1/prices/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(PriceResponse.class)
                .consumeWith(result -> {
                    Assert.assertNotNull(result.getResponseBody());
                    Assert.assertEquals(Long.valueOf(1), result.getResponseBody().getId());
                });
    }
}

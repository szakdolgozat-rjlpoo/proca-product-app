package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.app.domain.entity.Category;

class CategoryResponseConverterTest {

    @Test
    void convert() {
        Category category = Category.builder()
                .id(1L)
                .name("name")
                .build();

        CategoryResponseConverter converter = new CategoryResponseConverter();

        String actualResponse = converter.convert(category);

        String expectedResponse = "name";

        Assert.assertEquals(expectedResponse, actualResponse);
    }
}

package xyz.nergal.proca.product.app.service.convert.converter;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.app.domain.entity.Category;

class CategoryConverterTest {

    @Test
    void convert() {
        CategoryConverter converter = new CategoryConverter();

        Category actualCategory = converter.convert("name");

        Category expectedCategory = Category.builder()
                .name("name")
                .build();

        Assert.assertEquals(expectedCategory, actualCategory);
    }
}

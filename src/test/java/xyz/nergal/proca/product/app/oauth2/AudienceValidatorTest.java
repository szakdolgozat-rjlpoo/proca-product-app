package xyz.nergal.proca.product.app.oauth2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collections;

class AudienceValidatorTest {

    @Test
    void validateShouldSucceedWhenExpectedAudienceIsFound() {
        AudienceValidator audience = new AudienceValidator("audience");

        Jwt jwt = Jwt.withTokenValue("token")
                .header("alg", "none")
                .audience(Collections.singleton("audience"))
                .build();
        OAuth2TokenValidatorResult result = audience.validate(jwt);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    void validateShouldFailWhenExpectedAudienceIsMissing() {
        AudienceValidator audience = new AudienceValidator("audience");

        Jwt jwt = Jwt.withTokenValue("token")
                .header("alg", "none")
                .audience(Collections.singleton("none"))
                .build();
        OAuth2TokenValidatorResult result = audience.validate(jwt);

        Assert.assertTrue(result.hasErrors());
        Assert.assertEquals("The required audience is missing", result.getErrors().iterator().next().getDescription());
    }

    @Test
    void validateShouldFailWhenAudienceIsNull() {
        AudienceValidator audience = new AudienceValidator("audience");

        Jwt jwt = Jwt.withTokenValue("token")
                .header("alg", "none")
                .claim("", "")
                .build();
        OAuth2TokenValidatorResult result = audience.validate(jwt);

        Assert.assertTrue(result.hasErrors());
        Assert.assertEquals("The required audience is missing", result.getErrors().iterator().next().getDescription());
    }
}

package xyz.nergal.proca.product.app.controller;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.common.io.ResourceUtils;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.app.config.PagingJsonSerializationConfiguration;
import xyz.nergal.proca.product.app.config.ReactivePageableConfiguration;
import xyz.nergal.proca.product.app.config.SecurityConfiguration;
import xyz.nergal.proca.product.app.service.PriceService;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebFluxTest({SupplierPriceController.class})
@Import({
        PagingJsonSerializationConfiguration.class,
        SecurityConfiguration.class,
        ReactivePageableConfiguration.class,
})
@SuppressWarnings("squid:S1192")
class SupplierPriceControllerTest {

    @MockBean
    private PriceService priceService;

    @MockBean
    private ReactiveJwtDecoder jwtDecoder;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private WebTestClient client;

    @Test
    void indexShouldRespondNotFoundWhenPriceServiceReturnsEmptyMono() {
        Mockito.when(priceService.list(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void indexShouldRespondOkWhenPriceServiceReturnsNonEmptyMono() {
        Mockito.when(priceService.list(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(Page.empty()));

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void indexShouldRespondWithCorrectPayload() throws IOException {
        PriceResponse priceResponse = PriceResponse.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        List<PriceResponse> priceResponses = Collections.singletonList(priceResponse);
        Pageable pageable = PageRequest.of(0, 20, Sort.by("id"));
        Page<PriceResponse> page = new PageImpl<>(priceResponses, pageable, priceResponses.size());

        Mockito.when(priceService.list(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(page));

        String expectedJson = ResourceUtils.asString(resourceLoader.getResource("classpath:static/price-index-payload.json"));

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .consumeWith(entityExchangeResult -> {
                    try {
                        Assertions.assertNotNull(entityExchangeResult);
                        Assertions.assertNotNull(entityExchangeResult.getResponseBody());
                        JSONAssert.assertEquals(expectedJson, new String(entityExchangeResult.getResponseBody()), true);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    @Test
    void readShouldRespondNotFoundWhenPriceServiceReturnsEmptyMono() {
        Mockito.when(priceService.get(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices/1")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void readShouldRespondOkWhenPriceServiceReturnsNonEmptyMono() {
        Mockito.when(priceService.get(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(PriceResponse.builder().build()));

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices/1")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void readShouldRespondWithCorrectPayload() throws IOException {
        PriceResponse priceResponse = PriceResponse.builder()
                .id(1L)
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .supplier("object-id")
                .updatedAt(LocalDateTime.parse("1970-01-01T00:00:01"))
                .build();

        Mockito.when(priceService.get(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(priceResponse));

        String expectedJson = ResourceUtils.asString(resourceLoader.getResource("classpath:static/price-read-payload.json"));

        this.client.get()
                .uri("/products/1/suppliers/object-id/prices/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody().json(expectedJson);
    }

    @Test
    void createShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        this.client.post()
                .uri("/products/1/suppliers/object-id/prices")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondForbiddenWhenClientSendsBearerAuthWithScopeInvalid() {
        Jwt jwt = jwt().claim("scope", "").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.post()
                .uri("/products/1/suppliers/object-id/prices")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void createShouldRespondBadRequestWhenPayloadInvalid() {
        Jwt jwt = jwt().claim("scope", "price:create").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        PriceRequest priceRequest = PriceRequest.builder().build();

        this.client.post()
                .uri("/products/1/suppliers/object-id/prices")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(priceRequest)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void createShouldRespondCreatedWhenClientSendsCorrectBearerAndCorrectPayload() {
        Mockito.when(this.priceService.store(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(PriceResponse.builder().build()));

        Jwt jwt = jwt().claim("scope", "price:create").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        PriceRequest productRequest = PriceRequest.builder()
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .build();

        this.client.post()
                .uri("/products/1/suppliers/object-id/prices")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(productRequest)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    void updateShouldRespondForbiddenWhenClientDoesNotSendBearerAuth() {
        this.client.put()
                .uri("/products/1/suppliers/object-id/prices/1")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondForbiddenWhenWhenClientSendsCorrectBearerAndCorrectPayload() {
        Jwt jwt = jwt().claim("scope", "").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        this.client.put()
                .uri("/products/1/suppliers/object-id/prices/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void updateShouldRespondBadRequestWhenPayloadInvalid() {
        Jwt jwt = jwt().claim("scope", "price:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        PriceRequest priceRequest = PriceRequest.builder().build();

        this.client.put()
                .uri("/products/1/suppliers/object-id/prices/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(priceRequest)
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void updateShouldRespondNotFoundWhenProductServiceReturnsEmptyMono() {
        Mockito.when(this.priceService.update(
                ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.empty());

        Jwt jwt = jwt().claim("scope", "price:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        PriceRequest priceRequest = PriceRequest.builder()
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .build();

        this.client.put()
                .uri("/products/1/suppliers/object-id/prices/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(priceRequest)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void updateShouldRespondOkWhenClientSendsCorrectBearerAndCorrectPayload() {
        Mockito.when(this.priceService.update
                (ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(Mono.just(PriceResponse.builder().build()));

        Jwt jwt = jwt().claim("scope", "price:update").build();
        Mockito.when(this.jwtDecoder.decode(ArgumentMatchers.anyString())).thenReturn(Mono.just(jwt));

        PriceRequest priceRequest = PriceRequest.builder()
                .currencyCode("USD")
                .value(new BigDecimal("1.1"))
                .build();

        this.client.put()
                .uri("/products/1/suppliers/object-id/prices/1")
                .headers(headers -> headers.setBearerAuth(jwt.getTokenValue()))
                .bodyValue(priceRequest)
                .exchange()
                .expectStatus().isOk();
    }

    private Jwt.Builder jwt() {
        return Jwt.withTokenValue("token").header("alg", "none");
    }
}

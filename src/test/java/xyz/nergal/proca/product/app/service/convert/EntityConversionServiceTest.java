package xyz.nergal.proca.product.app.service.convert;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import xyz.nergal.proca.product.api.PriceRequest;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.api.ProductRequest;
import xyz.nergal.proca.product.api.ProductResponse;
import xyz.nergal.proca.product.app.domain.entity.Category;
import xyz.nergal.proca.product.app.domain.entity.Price;
import xyz.nergal.proca.product.app.domain.entity.Product;

class EntityConversionServiceTest {

    @Test
    void canConvertStringToCategory() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(String.class, Category.class));
    }

    @Test
    void canConvertPriceRequestToPrice() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(PriceRequest.class, Price.class));
    }

    @Test
    void canConvertProductRequestToProduct() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(ProductRequest.class, Product.class));
    }

    @Test
    void canConvertCategoryToString() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(Category.class, String.class));
    }

    @Test
    void canConvertPriceProductResponse() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(Price.class, PriceResponse.class));
    }

    @Test
    void canConvertProductToProductResponse() {
        EntityConversionService conversionService = new EntityConversionService();

        Assert.assertTrue(conversionService.canConvert(Product.class, ProductResponse.class));
    }
}

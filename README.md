# Product App for Proca

[![pipeline status](https://gitlab.com/szakdolgozat-rjlpoo/proca-product-app/badges/master/pipeline.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-product-app/commits/master)
[![coverage report](https://gitlab.com/szakdolgozat-rjlpoo/proca-product-app/badges/master/coverage.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-product-app/commits/master)

## Docker

By default Docker file exposes 8120 port.

## Endpoints

### Product endpoints

Read all

```
GET /products
Permit all

Query parameters:
- "page": 0
- "size": 1
- "sort": "id,ASC"

Response body:
{
    "content": {
        "id": 1,
        "name": "",
        "img": "",
        "description": "",
        "categories": [
            ""
        ],
        "prices": [
            {
                "id": 1
                "currencyCode": ""
                "value": 1.00
                "supplier": ""
                "updatedAt": "1970-01-01T00:00:01"
            }
        ]
    },
    "sort": [
        {
            "direction": "",
            "property": ""
        }
    ],
    "totalElements": 0,
    "size": 1,
    "number": 0
}
```

Read

```
GET /products/{product-id}
Permit all

Response body:
{
    "id": 1,
    "name": "",
    "img": "",
    "description": "",
    "categories": [
        ""
    ],
    "prices": [
        {
            "id": 1
            "currencyCode": ""
            "value": 1.00
            "supplier": ""
            "updatedAt": "1970-01-01T00:00:01"
        }
    ]
}
```

Create

```
POST /products
Scope product:create

Request body:
{
    "name": "",
    "img": "",
    "description": "",
    "categories": [
        ""
    ]
}

Response body:
{
    "id": 1,
    "name": "",
    "img": "",
    "description": "",
    "categories": [
        ""
    ],
    "prices": [
        {
            "id": 1
            "currencyCode": ""
            "value": 1.00
            "supplier": ""
            "updatedAt": "1970-01-01T00:00:01"
        }
    ]
}
```

Update

```
PUT /products/{product-id}
Scope product:update

Response body:
{
    "id": 1,
    "name": "",
    "img": "",
    "description": "",
    "categories": [
        ""
    ],
    "prices": [
        {
            "id": 1
            "currencyCode": ""
            "value": 1.00
            "supplier": ""
            "updatedAt": "1970-01-01T00:00:01"
        }
    ]
}
```

Delete

```
DELETE /products/{product-id}
Scope product:delete
```

### Price endpoints

Read all
```
GET /products/{product-id}/prices
Permit all

Query parameters:
- page
- size
- sort

Response body:
{
    "content": {
        "id": 1
        "currencyCode": ""
        "value": 1.00
        "supplier": ""
        "updatedAt": "1970-01-01T00:00:01"
    },
    "sort": [
        {
            "direction": "",
            "property": ""
        }
    ],
    "totalElements": 0,
    "size": 1,
    "number": 0
}
```

Read

```
GET /products/{product-id}/prices/{price-id}
Permit all

Response body:
{
    "id": 1
    "currencyCode": ""
    "value": 1.00
    "supplier": ""
    "updatedAt": "1970-01-01T00:00:01"
}
```

### Supplier price endpoints

Read all
```
GET /products/{product-id}/suppliers/{supplier-id}/prices
Permit all

Query parameters:
- page
- size
- sort

Response body:
{
    "content": {
        "id": 1
        "currencyCode": ""
        "value": 1.00
        "supplier": ""
        "updatedAt": "1970-01-01T00:00:01"
    },
    "sort": [
        {
            "direction": "",
            "property": ""
        }
    ],
    "totalElements": 0,
    "size": 1,
    "number": 0
}
```

Read

```
GET /products/{product-id}/suppliers/{supplier-id}/prices/{price-id}
Permit all

Response body:
{
    "id": 1
    "currencyCode": ""
    "value": 1.00
    "supplier": ""
    "updatedAt": "1970-01-01T00:00:01"
}
```

Create

```
POST /products/{product-id}/suppliers/{supplier-id}/prices
Scope price:create

Request body:
{
    "currencyCode": ""
    "value": 1.00
}

Response body:
{
    "id": 1
    "currencyCode": ""
    "value": 1.00
    "supplier": ""
    "updatedAt": "1970-01-01T00:00:01"
}
```

Update

```
PUT /products/{product-id}/suppliers/{supplier-id}/prices/{price-id}
Scope price:update

Request body:
{
    "currencyCode": ""
    "value": 1.00
}

Response body:
{
    "id": 1
    "currencyCode": ""
    "value": 1.00
    "supplier": ""
    "updatedAt": "1970-01-01T00:00:01"
}
```
